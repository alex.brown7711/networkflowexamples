#!/usr/bin/env python

import gurobipy as gp
from gurobipy import GRB

# Base data
nodes = [1, 2, 3, 4, 5, 6]

arcs, capacity = gp.multidict(
{
    (1, 2) : 1,
    (2, 4) : 1,
    (2, 5) : 1,
    (3, 1) : 1,
    (3, 2) : 1,
    (3, 5) : 1,
    (4, 3) : 1,
    (4, 5) : 1,
    (5, 6) : 1,
    (6, 4) : 1,
})

# Cost for triplets commodity-source-destination
cost = \
{
    (1, 2) : 1,
    (2, 4) : 5,
    (2, 5) : 9,
    (3, 1) : 1,
    (3, 2) : 8,
    (3, 5) : 6,
    (4, 3) : 2,
    (4, 5) : 7,
    (5, 6) : 1,
    (6, 4) : 2,
}

# Demand for pairs of commodity-city
inflow = \
{
    (1): -1,
    (2): 0,
    (3): 0,
    (4): 0,
    (5): 0,
    (6): 1,
}

# Create optimization model
m = gp.Model('netflow')

# Create variables
flow = m.addVars(arcs, obj=cost, name="flow")

# Arc-capacity constraints
for i, j in arcs:
    m.addConstr((flow[i, j]) <= capacity[i, j], "cap[%s, %s]" % (i, j))

# Flow-conservation constraints
m.addConstrs(
 (gp.quicksum(flow[i, j] for i, j in arcs.select('*', j)) + inflow[j] ==
   gp.quicksum(flow[j, k] for j, k in arcs.select(j, '*'))
   for j in nodes), "node")

# Compute optimal solution
m.optimize()

# Print solution
if m.status == GRB.OPTIMAL:
    solution = m.getAttr('x', flow)
    for i, j in arcs:
        if solution[i, j] > 0:
            print('%s -> %s: %g' % (i, j, solution[i, j]))
